import time

import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from monitoring_loader import loader
from monitoring_loader.config import config
from monitoring_loader import utils


def schedule_jobs():
    # sched = BackgroundScheduler()
    # sched_desj = utils.search('job_desjardins_imis', 'job_id', config['jobs'])['schedule']
    # sched.add_job(loader.job_desjardins_imis, **sched_desj)
    # # sched.add_job(loader.job_)
    # sched.start()
    # while True: time.sleep(10)
    pass


def run():
    if config['debug']['run_once']:
        loader.job_clusters("BBM_PROD")
    else:
        schedule_jobs()