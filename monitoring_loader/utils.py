import datetime
import pytz

def get_utc(pattern="%Y-%m-%d %H:%M:%S"):
    curr_time = datetime.datetime.utcnow()
    return curr_time.strftime(pattern)


def search(value='', key='', data=[]):
    """ 
        Performs a search in a list of dictionaries. 
        This function looks for dictionary values with the given `key` and matches with the given `value`.

        ```
        list_of_dictionaries = [
            {'name':'Pami', 'age':12},
            {'name':'Cris', 'age':21},
            {'name':'Ali', 'age':22}
        ]
        search(value="Cris", key="name", data=list_of_dictionaries)
        >>> [{'name':'Cris', 'age':21}]

        ```

        Params:
            value (str)     - The value to be searched
            key (str)       - The dictionary key where the value is assigned
            data (list)     - List of dictionaries where the search will be performed
    """
    return [element for element in data if element[key] == value][0]