from monitoring_loader.models.base import Base

class Node(Base):
    def __init__(self, **kwargs):
        super().__init__()
        NA = 'NA'
        INT_NA = 0

        self.cluster = kwargs.get('cluster', NA)
        self.node_name = kwargs.get('node_name', NA)
        self.node_ipaddress = kwargs.get('node_ipaddress', NA)
        self.node_status = kwargs.get('node_status', NA)
        self.node_disk_size = kwargs.get('node_disk_size', NA)
        self.node_disk_percentage = kwargs.get('node_disk_percentage', INT_NA)
        self.node_disk_total = kwargs.get('node_disk_total', INT_NA)
        self.node_disk_flag = kwargs.get('node_disk_flag', NA)
        self.node_type = kwargs.get('node_type', NA)
        

    def __repr__(self):
        return self.node_name

