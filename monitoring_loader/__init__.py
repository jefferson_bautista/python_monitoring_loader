from elasticsearch import Elasticsearch

from monitoring_loader.config import config
from monitoring_loader import utils

def init_desj_sql_server():
    return "SQL Server"

def init_pmpc_teradata():
    return "Teradata"

def init_elastic(url, **kwargs):
    return Elasticsearch(url, **kwargs)

sql_conn_desj = init_desj_sql_server()
tera_conn_pmpc = init_desj_sql_server()

_elastic_clusters = utils.search(value='BBM_PROD', key='cluster_name', data=config['connection']['elasticsearch'])
elastic_conn_efast = init_elastic(_elastic_clusters['url'])
